#include "scoresloader.h"
#include <QUrl>
#include <QFile>
#include <QFileDevice>
#include <QTextStream>

ScoresLoader::ScoresLoader(QObject *parent) : QThread(parent),
    mTextStream(nullptr), mFile(nullptr), mAbort(false)
{
}

ScoresLoader::~ScoresLoader() {

    if (mTextStream != nullptr) {
        delete mTextStream;
    }

    if (mFile != nullptr) {
        mFile->close();
        delete mFile;
    }
}

bool ScoresLoader::startLoading(const QString &path) {

    if (isRunning()) {
        return false;   /* Already running thread */
    }

    if (path.isEmpty()) {
        return false;
    }

    /* Path from QML comes as URI */

    mFile = new QFile(QUrl(path).toLocalFile());
    if (!mFile->exists() || !mFile->open(QIODevice::Text|QIODevice::ReadOnly)) {
        delete mFile;
        mFile = nullptr;
        return false;
    }

    mTextStream = new QTextStream(mFile);

    mAbort = false; /* To be sure if we are restarting */

    start();

    return true;
}

void ScoresLoader::run() {

    while (!mAbort && !mTextStream->atEnd()) {
        QString line = mTextStream->readLine();
        QStringList list = line.split(QRegExp("\\W+"), QString::SkipEmptyParts);
        for (auto &word : list) {
            emit wordExtracted(word);
        }
    }

    /* Report we are done */
    emit loadComplete();
}
