import QtQuick 2.12
import QtQuick.Dialogs 1.2

FileDialog {
    id: textFileDialog
    title: qsTr("Please choose a text file")
    folder: shortcuts.home
    nameFilters: [ qsTr("Text files (*.txt)"), qsTr("All files (*)") ]
}
