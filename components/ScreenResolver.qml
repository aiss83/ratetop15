pragma Singleton
import QtQuick 2.12
import aiss 1.0

Item {
    /* Pixel density function */
    function dp(x) {
        if (AppSettings.dpi < 120) {
            return x;
        } else {
            return x*(AppSettings.dpi/160);
        }
    }
}
