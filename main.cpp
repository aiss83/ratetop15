#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QScreen>
#include <appsettings.h>
#include <scoreboard.h>


int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    /* Set application parameters */
    QGuiApplication::setApplicationName("RateTop15");
    QGuiApplication::setApplicationVersion("1.0.0");
    QGuiApplication::setOrganizationName("AISS");

    qRegisterMetaType<ScoreRecord>();

    QQuickStyle::setStyle("Material");

    QQmlApplicationEngine engine;

    /* Register all additional types */
    qmlRegisterSingletonType<AppSettings>("aiss", 1, 0, "AppSettings", [](QQmlEngine *engine, QJSEngine *jsEngine) -> QObject * {
        Q_UNUSED(engine);
        Q_UNUSED(jsEngine);

        auto *apps = AppSettings::instance();
        auto *screen = QGuiApplication::screens().at(0);
        if (screen)
            apps->setDpi(screen->logicalDotsPerInch());

        return apps;
    });

    qmlRegisterType<ScoreBoard>("aiss", 1, 0, "ScoreBoard");
    qmlRegisterType<ScoreRecord>("aiss", 1, 0, "ScoreRecord");

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
