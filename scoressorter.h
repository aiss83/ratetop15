#ifndef SCORESSORTER_H
#define SCORESSORTER_H

#include <QThread>
#include <QMutex>
#include <scoresloader.h>


/**
 * @brief The ScoreRecord class model used to transfer scores to UI
 */
class ScoreRecord: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString caption READ caption NOTIFY captionChanged)
    Q_PROPERTY(quint64 score READ score NOTIFY scoreChanged)
public:

    explicit ScoreRecord(QObject *parent = nullptr);

    ScoreRecord(const QString &text, quint32 count, QObject *parent = nullptr);

    ScoreRecord(const ScoreRecord &other);

    ~ScoreRecord() = default;

    QString caption() const;

    quint32 score() const;

    void setText(QString text);

    void setCount(quint64 count);

    ScoreRecord& operator=(const ScoreRecord &other);

    static ScoreRecord* empty();

signals:

    void captionChanged();

    void scoreChanged();

private:

    QString mText;
    qint32  mCount;

    static ScoreRecord *mEmptyInstance;

};

Q_DECLARE_METATYPE(ScoreRecord)

/**
 * @brief The ScoresSorter class sort scores and post it to UI
 */
class ScoresSorter : public QThread
{
    Q_OBJECT
public:
    explicit ScoresSorter(QObject *parent = nullptr);

    ~ScoresSorter();

    /**
     * @brief beginSorting start sorting of words got from text file
     * @param path Path to the text file
     * @return
     */
    bool beginSorting(const QString &path);

    /**
     * @brief scores get to 15-n scores of words
     * @return
     */
    QVector<ScoreRecord> scores() const;

public slots:

    /**
     * @brief addWord
     * @param word
     */
    void addWord(QString word);

    /**
     * @brief completeLoad
     */
    void completeLoad();

signals:
    void scoresSorted();

    void complete();

protected:
    void run() override;

    void sortHash();

private:
    QHash<QString, quint64>  *mWordsTable;
    bool                      mAbort;
    ScoresLoader             *mLoader;
    QString                   mPath;
    QVector<ScoreRecord>     *mScores;
    mutable QMutex            mMutex;
};

#endif // SCORESSORTER_H
