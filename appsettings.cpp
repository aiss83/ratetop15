#include "appsettings.h"

const int DEFAULT_DPI = 120;

AppSettings* AppSettings::mInstance = nullptr;

AppSettings::AppSettings(QObject *parent) : QObject(parent),
                                      mDpi(DEFAULT_DPI)
{

}

int AppSettings::dpi() const {
    return mDpi;
}

void AppSettings::setDpi(int dpi) {
    assert(dpi > 0);
    mDpi = dpi;

    emit dpiChanged();
}

AppSettings* AppSettings::instance() {
    if (mInstance == nullptr)
        mInstance = new AppSettings();

    return mInstance;
}
