#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QObject>

/**
 * @brief The AppModel class provides basic information on DPI and other parameters
 */
class AppSettings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int dpi READ dpi WRITE setDpi NOTIFY dpiChanged);
public:

    /**
     * @brief setDpi Set DPI value
     * @param dpi
     */
    void setDpi(int dpi);

    /**
     * @brief dpi Get DPI value
     * @return
     */
    int dpi() const;

    /**
     * @brief instance AppSettings instance
     * @return Instance of singleton
     */
    static AppSettings* instance();

protected:
    explicit AppSettings(QObject *parent = nullptr);
    ~AppSettings() = default;

signals:

    void dpiChanged();

private:

    int mDpi;
    static AppSettings *mInstance;

};

#endif // APPSETTINGS_H
