#include "scoressorter.h"
#include <QHash>
#include <algorithm>
#include <QMutexLocker>
#include <QVector>


static const int TOP_RATING = 15;

ScoreRecord* ScoreRecord::mEmptyInstance = nullptr;


ScoreRecord::ScoreRecord(QObject *parent) : QObject(parent), mText(QString()), mCount(0) {
}

ScoreRecord::ScoreRecord(const QString &text, quint32 count, QObject *parent): QObject(parent), mText(text), mCount(count) {
}

ScoreRecord::ScoreRecord(const ScoreRecord &other) : QObject(other.parent()) {
    mCount = other.mCount;
    mText = other.mText;
}

ScoreRecord* ScoreRecord::empty() {
    if (mEmptyInstance == nullptr) {
        mEmptyInstance = new ScoreRecord();
    }

    return mEmptyInstance;
}

quint32 ScoreRecord::score() const {
    return mCount;
}

QString ScoreRecord::caption() const {
    return mText;
}

void ScoreRecord::setCount(quint64 count) {
    mCount = count;
}

void ScoreRecord::setText(QString text) {
    mText = text;
}

ScoreRecord& ScoreRecord::operator=(const ScoreRecord &other) {
    if (this != &other) {
        mCount = other.mCount;
        mText = other.mText;
        setParent(other.parent());
    }

    return *this;
}


ScoresSorter::ScoresSorter(QObject *parent) : QThread(parent),
    mWordsTable(nullptr), mAbort(false), mLoader(nullptr), mPath(QString()), mScores(nullptr), mMutex(QMutex::Recursive)
{
    mWordsTable = new QHash<QString, quint64>();
    mScores = new QVector<ScoreRecord>();
    mLoader = new ScoresLoader(this);

    QObject::connect(mLoader, &ScoresLoader::wordExtracted, this, &ScoresSorter::addWord, Qt::BlockingQueuedConnection);
    QObject::connect(mLoader, &ScoresLoader::loadComplete, this, &ScoresSorter::completeLoad);
}

ScoresSorter::~ScoresSorter() {
    mAbort = true;
}

bool ScoresSorter::beginSorting(const QString &path) {
    if (mLoader == nullptr)
        return false;

    mPath = path;

    if (isRunning()) {
        return false;
    } else {
        mAbort = false;
        start();
    }

    return true;
}

QVector<ScoreRecord> ScoresSorter::scores() const {
    QMutexLocker locker(&mMutex);

    auto sz = mScores->size();

    if (sz < TOP_RATING) {
        return mScores->mid(0);
    } else {
        return mScores->mid(0, TOP_RATING);
    }
}

void ScoresSorter::addWord(QString word) {
    (*mWordsTable)[word]++; /* This will count all the words */
}

void ScoresSorter::completeLoad() {
    mAbort = true;  /* ready to finish */
    sortHash(); /* Sort final result */
}

void ScoresSorter::sortHash() {

    if (mWordsTable->size() > 0) {
        QVector<ScoreRecord> scores;

        for (auto it = mWordsTable->constBegin(); it != mWordsTable->constEnd(); ++it) {
            scores.append(ScoreRecord(it.key(), it.value()));
        }

        std::sort(scores.begin(), scores.end(), [](ScoreRecord a, ScoreRecord b) { return a.score() > b.score(); });

        QMutexLocker locker(&mMutex);
        mScores->clear();
        mScores->append(scores);

        emit scoresSorted();
    }
}

void ScoresSorter::run() {

    if (!mLoader->startLoading(mPath)) {
        return; /* Path wasn't created */
    }

    while(!mAbort) {
        sortHash();
        /* Perform sort of scores */
        msleep(10); /* yeld CPU */
    }

    sortHash(); /* Do it last time */
    emit complete();
}
