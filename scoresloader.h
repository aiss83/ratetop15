#ifndef SCORESLOADER_H
#define SCORESLOADER_H

#include <QThread>

class QTextStream;
class QFile;

/**
 * @brief The ScoresLoader class loads texts from file and provides initial parsing of strings
 */
class ScoresLoader : public QThread
{
    Q_OBJECT
public:
    /**
     * @brief ScoresLoader
     * @param parent
     */
    explicit ScoresLoader(QObject *parent = nullptr);

    /**
     *
     */
    ~ScoresLoader();

    /**
     * @brief startLoading
     * @param path
     * @return
     */
    bool startLoading(const QString &path);

signals:
    /**
     * @brief loadComplete raised when loading process complete
     */
    void loadComplete();

    /**
     * @brief wordExtracted raised when word being extracted
     */
    void wordExtracted(QString);

protected:
    void run() override;

private:
    QTextStream *mTextStream;
    QFile       *mFile;
    bool         mAbort;
};

#endif // SCORESLOADER_H
