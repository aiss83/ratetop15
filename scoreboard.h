#ifndef SCOREBOARD_H
#define SCOREBOARD_H

#include <QObject>
#include <scoressorter.h>
#include <QQmlListProperty>

class ScoreBoard : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<ScoreRecord> scores READ scores NOTIFY scoresChanged)
    Q_PROPERTY(bool processing READ processing NOTIFY processingChanged)

public:

    /**
     * @brief ScoreBoard
     * @param parent
     */
    explicit ScoreBoard(QObject *parent = nullptr);

    ~ScoreBoard();

    /**
     * @brief buildScore starts building of scoreboard in two separate threads
     * @param path Path to the text file
     * @return true on successful process start
     */
    Q_INVOKABLE bool buildScore(const QString &path);

    /**
     * @brief setScores
     * @param scores
     */
    void setScores(const QVector<ScoreRecord> &scores);

    /**
     * @brief processing Check is processing
     * @return
     */
    bool processing() const;

    /**
     * @brief scores get scores data to represent
     * @return
     */
    QQmlListProperty<ScoreRecord> scores();

signals:

    /**
     * @brief scoresUpdate on scores table being updated
     */
    void scoresUpdate();

    /**
     * @brief complete overall process
     */
    void complete();

    /**
     * @brief scoresChanged when scores data changed
     */
    void scoresChanged();

    /**
     * @brief processingChanged
     */
    void processingChanged();

public slots:

    void scoresSorted();

private:

    ScoresSorter            *mSorter;
    QVector<ScoreRecord*>   *mScores;
    QQmlListProperty<ScoreRecord> *mScoresList;

};

#endif // SCOREBOARD_H
