import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import aiss 1.0
import "components"


ApplicationWindow {
    id: mainWindow
    width: ScreenResolver.dp(800)
    height: ScreenResolver.dp(600)
    visible: true
    title: qsTr("Rate 15-Top")

    ScoreBoard {
        id: scoreBoard
    }

    TextFileDialog {
        id: textFileDialog
        onAccepted: {
            /* Set URL to model */
            scoreBoard.buildScore(textFileDialog.fileUrl)
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: ScreenResolver.dp(3)
        spacing: ScreenResolver.dp(5)

        /* Overall progress */
        ProgressBar {
            id: loadProgress
            Layout.fillWidth: true
            indeterminate: true
            visible: scoreBoard.processing
        }

        /* Charts area */
        ListView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            interactive: contentHeight > height
            model: scoreBoard.scores
            spacing: ScreenResolver.dp(3)
            delegate: Rectangle {
                width: { score > 0 ? ScreenResolver.dp(score * 20) : ScreenResolver.dp(2) }
                gradient: Gradient.YoungPassion
                height: ScreenResolver.dp(30)
                radius: ScreenResolver.dp(3)
                Text {
                    padding: ScreenResolver.dp(5)
                    verticalAlignment: Text.AlignVCenter
                    text: { score > 0 ? caption + " (" + score + ")" : "" }
                    color: Material.foreground
                }
            }
        }

        /* Button and progress area */
        Button {
            id: openButton
            Layout.alignment: Layout.Center
            text: qsTr("Open file...")
            onClicked: {
                textFileDialog.open()
            }
        }
    }
}
