#include "scoreboard.h"
#include <QVector>

static const int SCORES_MAX_COUNT = 15;

/* Not used from QML */
static void scoreboardAppend(QQmlListProperty<ScoreRecord> *prop, ScoreRecord *val)
{
    Q_UNUSED(val);
    Q_UNUSED(prop);
}

static ScoreRecord* scoreboardtAt(QQmlListProperty<ScoreRecord> *prop, int index)
{
    auto *d = reinterpret_cast<QVector<ScoreRecord*>*>(prop->data);
    if (index < d->size()) {
        auto *r = d->at(index);
        if (r) {
            return r;
        }
    }

    return ScoreRecord::empty();
}

static int scoreboardCount(QQmlListProperty<ScoreRecord> *prop)
{
    auto *d = reinterpret_cast<QVector<ScoreRecord>*>(prop->data);
    return d->size();
}

static void scoreboardClear(QQmlListProperty<ScoreRecord> *prop)
{
    Q_UNUSED(prop)
}


ScoreBoard::ScoreBoard(QObject *parent) : QObject(parent),
    mSorter(nullptr), mScores(new QVector<ScoreRecord*>(SCORES_MAX_COUNT)), mScoresList(nullptr)
{
    mScoresList = new QQmlListProperty<ScoreRecord>(this, mScores, scoreboardAppend,
                                                                   scoreboardCount,
                                                                   scoreboardtAt,
                                                                   scoreboardClear);
}

ScoreBoard::~ScoreBoard() {
    if (mScores != nullptr) {
        delete mScores;
        mScores = nullptr;
    }
    if (mSorter != nullptr) {
        delete mSorter;
        mSorter = nullptr;
    }
}

bool ScoreBoard::processing() const {
    return (mSorter != nullptr) ? !mSorter->isFinished() : false;
}

bool ScoreBoard::buildScore(const QString &path) {
    if (mSorter == nullptr)
        mSorter = new ScoresSorter(this);

    QObject::connect(mSorter, &ScoresSorter::scoresSorted, this, &ScoreBoard::scoresSorted);
    QObject::connect(mSorter, &ScoresSorter::finished, this, [=]() {
        emit processingChanged();
        emit complete(); });

    bool res = mSorter->beginSorting(path);
    emit processingChanged();
    return res;
}

void ScoreBoard::scoresSorted() {
    auto scores = mSorter->scores();

    setScores(scores);
}

void ScoreBoard::setScores(const QVector<ScoreRecord> &scores) {
    int maxCount = scores.size() > SCORES_MAX_COUNT ? SCORES_MAX_COUNT : scores.size();
    for(int i = 0; i < maxCount; ++i) {
        if (mScores->at(i) != nullptr) {
            delete mScores->at(i);
        }
        mScores->replace(i, new ScoreRecord(scores.at(i)));
    }

    emit scoresChanged();
}

QQmlListProperty<ScoreRecord> ScoreBoard::scores() {

    return *mScoresList;
}

